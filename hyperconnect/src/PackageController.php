<?php

namespace HyperConnect;

class PackageController extends Service
{

    /**
     * List all hosting packages attached to this token
     */
    public function index()
    {
        return $this->client->get('/api/package');
    }

    /**
     * Get single package by id
     */
    public function show($packageId)
    {
        return $this->client->get('/api/package/' . $packageId);
    }

    /**
     * Create a new package
     */
    public function store()
    {
        return $this->client->post('/api/package');
    }

    /**
     * Destroy package
     */
    public function destroy($packageId)
    {
        return $this->client->delete('/api/package/' . $packageId);
    }

}