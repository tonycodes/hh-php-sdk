<?php

namespace HyperConnect;

class TeamController extends Service
{
    /**
     * List all hosting packages attached to this token
     */
    public function index()
    {
        return $this->client->get('/api/team');
    }

    /**
     * Get single package by id
     */
    public function show($teamId)
    {
        return $this->client->get('/api/team/' . $teamId);
    }

    /**
     * Create a new package
     */
    public function store()
    {
        return $this->client->post('/api/team');
    }

    /**
     * Destroy package
     */
    public function destroy($teamId)
    {
        return $this->client->delete('/api/team/' . $teamId);
    }

}