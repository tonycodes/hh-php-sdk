<?php

namespace HyperConnect;

use GuzzleHttp\Client;

class Service
{

    protected $client;

    public function __construct()
    {
        /**
         * Setup a Guzzle Client just for this Auth request
         */
        $this->client = new Client([
            'base_uri' => 'https://hyper.host/api/',
            'query' => ['api_token' => getenv('HYPER_HOST_TOKEN')]

        ]);
    }

    /**
     * @param $endPoint
     *
     * @return mixed
     */
    public function get($endPoint)
    {
        return $this->client->get($endPoint);
    }

    /**
     * @param $endPoint
     * @param $payLoad
     *
     * @return mixed
     */
    public function post($endPoint, $payLoad)
    {
        return $this->client->post($endPoint, $payLoad);
    }

    /**
     * @param $endPoint
     * @param $payLoad
     *
     * @return mixed
     */
    public function delete($endPoint, $payLoad)
    {
        return $this->client->delete($endPoint, $payLoad);
    }

    /**
     * @param $endPoint
     * @param $payLoad
     *
     * @return mixed
     */
    public function put($endPoint, $payLoad)
    {
        return $this->client->put($endPoint, $payLoad);
    }

}