<?php

namespace HyperConnect;

class MigrationController extends Service
{
    /**
     * List all hosting packages attached to this token
     */
    public function index()
    {
        return $this->client->get('/api/migration');
    }

    /**
     * Get single package by id
     */
    public function show($migrationId)
    {
        return $this->client->get('/api/migration/' . $migrationId);
    }

    /**
     * Create a new package
     */
    public function store()
    {
        return $this->client->post('/api/migration');
    }

    /**
     * Destroy package
     */
    public function destroy($migrationId)
    {
        return $this->client->delete('/api/migration/' . $migrationId);
    }

}